# [trying-fricas](https://gitlab.com/eidoom/trying-fricas)

* [docs](https://fricas.github.io)
* [source](https://github.com/fricas/fricas)

Download [latest release](https://github.com/fricas/fricas/releases/) and install:

```shell
sudo dnf install sbcl
mkdir -p ~/build/fricas
cd ~/build/fricas
wget https://github.com/fricas/fricas/releases/download/${VERSION}/fricas-${VERSION}-full.tar.bz2}
tar xf fricas-${VERSION}-full.tar.bz2
cd fricas-${VERSION}
mkdir build
cd build
../configure --prefix=$HOME/local/fricas
make -j`nproc`
make install
echo "export PATH=$HOME/local/fricas/bin:$PATH" >> ~/.<shell>rc
```
